capture program drop RegressionDiscontinuityCV
program define RegressionDiscontinuityCV, rclass
#delim ;	
	syntax 
		varlist(min=2 max=2 numeric)
		[if] [in]
		,
		[
			kernel(string)
			p(integer 1)
			c(real 0)
			GRIDlist(numlist sort)
			covars(varlist numeric fv ts)
			folds(integer 10)
		]
	;
#delim cr
	**Obtain the running var and outcome var
	tokenize `varlist'
	local y `1'
	local rv `2'
	
	marksample touse
	if "`covars'" != "" {
		markout `touse' `covars'
	}
	
	**Generate a numeric foldID indicator
	**It will me missing for all observations not in `touse'
	tempvar rand foldid
	qui gen double `rand' = runiform() if `touse'
	qui xtile `foldid' = `rand' if `touse', nquantiles(`folds')
	drop `rand'
	
	**Compute bandwidths on the left and right
	foreach side in l r {
		if "`side'" == "l" {
			local gl >
			local sideDesc left
		}
		else {
			local gl <
			local sideDesc right
		}
		
		**Loop through each candidate BW in gridlist and compute the MSE 
		**via cross-validation
		
		
		
	}
	
	
	
end

capture program drop RegressionDiscontinuityCV_MSE
program define RegressionDiscontinuityCV, rclass
	
	
	******************************
	** Generate kernel weights
	*******************************
	tempvar w
	gen `w' = 0
	if "`kernel'" == "uniform" {
		**Uniform Kernel
		qui replace `w' = inrange(`rv', `c'-`lbwidth', `c'+`rbwidth') & `if'
	}
	else if "`kernel'" == "triangle" {
		**Triangle kernel
		qui replace `w' = 1 - abs( (`c'-`rv')/`lbwidth' )  if inrange( (`c'-`rv')/`lbwidth', 0, 1) & `if'
		qui replace `w' = 1 - abs( (`c'-`rv')/`rbwidth' )  if inrange( (`c'-`rv')/`rbwidth', -1, 0) & `if'
	}
	else {
		di "{err}Unknown kernel {bf:`kernel'}"
		exit 99
	}
	**If we specified a weight, multiply it by the kernel weight
	if "`exp'" != "" {
		qui replace `w' = `w' * `exp'
	}
	
	*****************************
	** Generate local polynomial trends
	** and compile a macro containing them
	*****************************
	local plist 
	forvalues i=1/`=`p'' {
		tempvar pl`i' pr`i'
		**Note trends should have value zero at the discontinuity
		qui gen `pl`i'' = cond(`rv'<`c',`rv'-`c',0)^`i' if `if'
		qui gen `pr`i'' = cond(`rv'>`c',`rv'-`c',0)^`i' if `if'
		local plist : list plist | pl`i'
		local plist : list plist | pr`i'
	}
	
	***********************************
	** Generate the treatment indicator
	***********************************
	tempvar d
	qui gen byte `d' = `rv' > `c' if `if'

	****************************
	** Estimate the RD
	****************************
	qui reg `y' `covars' `plist' `d' [aw=`w']

	if _b[`d'] == 0 & _se[`d'] == 0 {
		di "{err}Insufficient data to compute estimates"
		exit 99
	}
	else {
		**return the parameter estimate to the caller
		return scalar b = _b[`d']
	}
		
	***************************
	** Count observations included in the regression
	** We don't need to do this when we're bootstrapping
	** so make it optional
	***************************
	if "`count'" != "nocount" {
		qui count if  `w' != 0 & `rv' < `c'
		return scalar N_l = r(N)

		qui count if  `w' != 0 & `rv' > `c' & `if' 
		return scalar N_r = r(N)
	}
