capture program drop RegressionDiscontinuity
program define RegressionDiscontinuity, eclass
#delim ;
	syntax varlist(min=2 max=2 numeric)
		[if] [in]
		, 
		[
			c(real 0)
			bwidth(numlist min=1 max=2 sort)
			p(numlist integer min=1 max=2 >=0 )
			kernel(string)
			COVariates(varlist fv ts)
			vce(string asis)
			bsreps(integer 100)
			sametrends
		]
	;
#delim cr

	di 
	di "{txt}Bootstrapped Regression Discontinuity"
	
	marksample touse
	
	if "`kernel'" == "" {
		local kernel triangle
	}
	di "{txt}Kernel: {res}`kernel'"
	
	tokenize `varlist'
	local depvar `1'
	local rv `2'
	di "{txt}Dependent Variable: {res}`depvar'"
	di "{txt}Running Variable: {res}`rv'"
	di "{txt}Threhold: {res}`c'"
	
	tempvar LB LSE RB RSE RV
	tempname LN RN B V p_l_act p_r_act
	**We only want to estimate lpolys at the discontinuity
	qui gen `RV' = `c' in 1
	
	local bwcount : word count `bwidth'
	if `bwcount' == 0 {
		local LBW 
		local RBW
		di "{txt}Bandwidth: {res}Automatic"
	}
	else if `bwcount' == 1 {
		local LBW `bwidth'
		local RBW `bwidth'
		di "{txt}Bandwidth: {res}`bwidth'"
	}
	else {
		tokenize `bwidth'
		local LBW `1'
		local RBW `2'
		di "{txt}Left Bandwidth: {res}`LBW'"
		di "{txt}Right Bandwidth: {res}`RBW'"
	}
	
	**Parse out the polynomial degrees
	if "`p'" == "" {
		di "{txt}Using default polynomial degrees"
		local p_l 1
		local p_r 1
	}
	else {
		local pcount : word count `p'
		if `pcount' == 1 {
			local p_l `p'
			local p_r `p'
		}
		else if `pcount' == 2 {
			tokenize `p'
			local p_l `1'
			local p_r `2'
			if "`sametrends'" == "sametrends" & `1' != `2' {
				di "{err}You cannot specifiy both {bf:sametrends} and distinct local polynomial trends for each side of the discontinuity" _cont
				di "using the {bf:p()} option"
				exit 99
			}
		}
		else {
			di "{err}Invalid polynomial degree: {res}`p'"
			exit 99
		}
	}
	di "{txt}Local Polynomial Degree Left: {res}`p_l'"
	di "{txt}Local Polynomial Degree Right: {res}`p_r'"
	
	
	**If we specified covariates, partial them out of Y
	if "`covariates'" != "" {
		di "{txt}Covariates: {res}`covariates'"
	}
	else {
		**Do nothing
	}
	
	
	**If we specified a cluster varlist, group on that
	if "`vce'" != "" {
		local vcetype : word 1 of `vce'

		if "`vcetype'" == "cluster" {
			tokenize `vce'
			macro shift
			local cluster `*'
			markout `touse' `cluster'
			local clusterVarCount : word count `cluster'
		}
		else {
			local vce vce(`vce')
			local clusterVarCount 0
		}
	}
	else {
		local vce 
		local clusterVarCount 0
	}	
	
	********************************
	** Compute local polynomial regresssions
	** You could probably make this way faster by doing it in Mata.
	** Generate kernel weights for each observation then compute the regression
	********************************	
	RegressionDiscontinuity_reg `depvar' `rv' if `touse', c(`c') p(`p_l' `p_r') ///
		kernel(`kernel') lbwidth(`LBW') rbwidth(`RBW') covars(`covariates') pfail `sametrends'
		
	scalar `LN' = r(N_l)
	scalar `RN' = r(N_r)
	matrix `B' = r(b)
	scalar `p_l_act' = r(p_l)
	scalar `p_r_act' = r(p_r)
	
	/*tempname LSE LB RSE RB
	qui lpoly `y' `rv' if `touse' & `rv' < `c', gen(`LB') at(`RV') se(`LSE') nograph bwidth(`LBW') degree(`p') kernel(`kernel')
	qui count if inrange(`rv',`c'-`LBW',`c')
	scalar `LN' = r(N)

	qui lpoly `y' `rv' if `touse' & `rv' > `c', gen(`RB') at(`RV') se(`RSE') nograph bwidth(`RBW') degree(`p') kernel(`kernel')
	qui count if inrange(`rv',`c',`c'+`RBW')
	scalar `RN' = r(N)
	di `=`RB'[1]' - `=`LB'[1]'
	*/
	
	
	**Now we bootstrap the procedure
	tempname BSVALS BSWEIGHTS BVAR BCOUNT
	qui gen int `BSWEIGHTS' = 1
	
	**********************************************
	** No clustering variable
	**********************************************
	if `clusterVarCount' == 0 {
		di "{txt}Bootstrapping robust variance matrix"
	
		scalar `BVAR' = 0
		scalar `BCOUNT' = 0
		di "{txt}Bootstrap Replications:"
		forvalues bsrep=1/`=`bsreps'' {
			qui bsample if `touse', weight(`BSWEIGHTS')
			
			capture RegressionDiscontinuity_reg `depvar' `rv' if `touse' [fw=`BSWEIGHTS'], ///
				c(`c') p(`p_l' `p_r') kernel(`kernel') lbwidth(`LBW') rbwidth(`RBW') nocount covars(`covariates') pfail `sametrends'
			
			if _rc == 0 {
				if r(b) < . {
					scalar `BVAR' = `BVAR' + (`B'[1,1]-r(b))^2
					scalar `BCOUNT' = `BCOUNT' + 1
					local dot {txt}.
				}
				else {
					local dot {err}x
				}
			}
			else {
				local dot {err}x
			}
			
			if mod(`bsrep',50) == 1 {
				di _n "{txt}" string(`bsrep'-1,"%-6.0f") " | `dot'" _continue
			}
			else {
				di "`dot'" _cont
			}
		}
		di
		matrix `V' = `BVAR'/(`BCOUNT'-1)
	}
	**********************************************
	** Single clustering variable
	**********************************************	
	else if `clusterVarCount' == 1 {
		di "{txt}Bootstrapping cluster-robust variance matrix"
	
		scalar `BVAR' = 0
		scalar `BCOUNT' = 0
		di "{txt}Bootstrap Replications:"
		forvalues bsrep=1/`=`bsreps'' {
			qui bsample if `touse', weight(`BSWEIGHTS') cluster(`cluster')
			
			capture RegressionDiscontinuity_reg `depvar' `rv' if `touse' [fw=`BSWEIGHTS'], ///
				c(`c') p(`p_l' `p_r') kernel(`kernel') lbwidth(`LBW') rbwidth(`RBW') nocount covars(`covariates') pfail `sametrends'
			
			if _rc == 0 {
				if r(b) < . {
					scalar `BVAR' = `BVAR' + (`B'[1,1]-r(b))^2
					scalar `BCOUNT' = `BCOUNT' + 1
					local dot {txt}.
				}
				else {
					local dot {err}x
				}
			}
			else {
				local dot {err}x
			}
			
			if mod(`bsrep',50) == 1 {
				di _n "{txt}" string(`bsrep'-1,"%-6.0f") " | `dot'" _continue
			}
			else {
				di "`dot'" _cont
			}
		}
		di
		matrix `V' = `BVAR'/(`BCOUNT'-1)	
	}
	**********************************************
	** Multiple clustering variable
	**********************************************	
	else if `clusterVarCount' == 2 {
		di "{txt}Bootstrapping two-way cluster-robust variance matrix"
	
		tempname interaction
		egen `interaction' = group(`cluster') if `touse'
		
		matrix `V' = 0
		foreach cv of varlist `cluster' `interaction' {
			scalar `BVAR' = 0
			scalar `BCOUNT' = 0
			di "{txt}Bootstrap Replications stage {res}`cv':"
			forvalues bsrep=1/`=`bsreps'' {
				qui bsample if `touse', weight(`BSWEIGHTS') cluster(`cv')
				
				capture RegressionDiscontinuity_reg `depvar' `rv' if `touse' [fw=`BSWEIGHTS'], ///
					c(`c') p(`p_l' `p_r') kernel(`kernel') lbwidth(`LBW') rbwidth(`RBW') ///
					nocount covars(`covariates') pfail `sametrends'
				
				if _rc == 0 {
					if r(b) < . {
						scalar `BVAR' = `BVAR' + (`B'[1,1]-r(b))^2
						scalar `BCOUNT' = `BCOUNT' + 1
						local dot {txt}.
					}
					else {
						local dot {err}x
					}
				}
				else {
					local dot {err}x
				}
				
				if mod(`bsrep',50) == 1 {
					di _n "{txt}" string(`bsrep'-1,"%-6.0f") " | `dot'" _continue
				}
				else {
					di "`dot'" _cont
				}
			}
			**Variance matrix (just a scalar here) is V(1) + V(2) - V(1&2)
			if "`cv'" == "`interaction'" matrix `V' = `V' - `BVAR'/(`BCOUNT'-1)
			else matrix `V' = `V' + `BVAR'/(`BCOUNT'-1)
			di _n _n
		}
	}
	
	*********************************
	** Test equality of the estimates at the discontinuity
	*********************************
	*qui ttesti `=`RN'' `=`RB'[1]' `=`RSE'[1]' `=`LN'' `=`LB'[1]' `=`LSE'[1]', unequal
	
	**********************
	**Extract results
	**********************	
	matrix rownames `B' = `depvar'
	matrix colnames `B' = LATE
	
	matrix rownames `V' = LATE
	matrix colnames `V' = LATE
	
	ereturn post `B' `V', depname(`depvar') esample(`touse') //obs(`=`RN'+`LN'')
	ereturn local cmd RegressionDiscontinuity
	ereturn scalar N_l = `LN'
	ereturn scalar N_r = `RN'
	ereturn scalar N_h_l = `LN'
	ereturn scalar N_h_r = `RN'
	ereturn scalar h_l = `LBW'
	ereturn scalar h_r = `RBW'
	
	ereturn display
	
end

capture program drop RegressionDiscontinuity_reg
program define RegressionDiscontinuity_reg, rclass
	#delim ;
		syntax varlist(min=2 max=2 numeric) if/ [fw/], 
			c(real) 
			p(numlist >=0 min=2 max=2 integer) 
			kernel(string) 
			lbwidth(real) 
			rbwidth(real) 
			[
				covars(varlist fv ts numeric)
				nocount
				pfail	/* Exit with an error if polynomial trends are unidentified */
				sametrends	/* use idential local polynomial trends on each side of the discontinuity */
			]
		;
	#delim cr
	tokenize `varlist'
	local y `1'
	local rv `2'
	
	******************************
	** Generate kernel weights
	*******************************
	tempvar w
	gen `w' = 0
	if "`kernel'" == "uniform" {
		**Uniform Kernel
		qui replace `w' = inrange(`rv', `c'-`lbwidth', `c'+`rbwidth') & `if'
	}
	else if "`kernel'" == "triangle" {
		**Triangle kernel
		qui replace `w' = 1 - abs( (`c'-`rv')/`lbwidth' )  if inrange( (`c'-`rv')/`lbwidth', 0, 1) & `if'
		qui replace `w' = 1 - abs( (`c'-`rv')/`rbwidth' )  if inrange( (`c'-`rv')/`rbwidth', -1, 0) & `if'
	}
	else {
		di "{err}Unknown kernel {bf:`kernel'}"
		exit 99
	}
	
	**If we specified a weight, multiply it by the kernel weight
	**I expect the user-specified weights to be bootstrap fweights
	if "`exp'" != "" {
		qui replace `w' = `w' * `exp'
	}
	
	*****************************
	** Generate local polynomial trends
	** and compile a macro containing them
	*****************************
	tokenize `p'
	local p_l `1'
	local p_r `2'
	local plist_l
	local plist_r
	
	if "`sametrends'" == "sametrends" {
		**If we specify "sametrends" then only compute one set of trends
		**And dont' constrain them. We'll just ignore p_r (by setting to zero) and plist_r
		local p_r 0
		forvalues i=1/`=`p_l'' {
			tempvar pl`i'
			**Note trends should have value zero at the discontinuity
			qui gen `pl`i'' = (`rv'-`c')^`i' if `if'
			local plist_l : list plist_l | pl`i'			
		}
	}
	else {
		forvalues i=1/`=`p_l'' {
			tempvar pl`i'
			**Note trends should have value zero at the discontinuity
			qui gen `pl`i'' = cond(`rv'<`c',`rv'-`c',0)^`i' if `if'
			local plist_l : list plist_l | pl`i'
		}
		
		forvalues i=1/`=`p_r'' {
			tempvar pr`i'
			**Note trends should have value zero at the discontinuity
			qui gen `pr`i'' = cond(`rv'>`c',`rv'-`c',0)^`i' if `if'
			local plist_r : list plist_r | pr`i'
		}
	}
	
	***********************************
	** Generate the treatment indicator
	***********************************
	tempvar d
	qui gen byte `d' = `rv' > `c' if `if'

	****************************
	** Estimate the RD
	****************************
	qui reg `y' `covars' `plist_l' `plist_r' `d' [aw=`w']

	**********************************
	** Check whether the local polynomial coefficients
	** are identified. We count the non-zero coefficients on the polynomia
	** trends and compare the the number requested by the user
	**********************************
	foreach s in r l {
		tempname plist_`s'_vcount 
		scalar `plist_`s'_vcount' = 0
		foreach p of local plist_`s' {
			if _b[`p'] != 0 | _se[`p'] != 0 {
				scalar `plist_`s'_vcount' = `plist_`s'_vcount' + 1
			}
		}
		if `plist_`s'_vcount' < `p_`s'' {
			if "`s'" == "l" local sdesc left
			else local sdesc right
			
			if "`pfail'" == "" {
				di "{txt}{bf:WARNING}: The localy polynomial trends to the `sdesc' of the discontinuity are not fully identified"
				di "{txt}The likely cause is insufficient variation in the running variable"
			}
			else {
				di "{err}{bf:ERROR}: The localy polynomial trends to the `sdesc' of the discontinuity are not fully identified"
				di "{err}The likely cause is insufficient variation in the running variable"
				di "{txt}Left (`p_l'): {res}`plist_l'"
				di "{txt}Right (`p_r'): {res}`plist_r'"
				reg
				exit 99
			}
			
			return scalar p_`s'_warn = 1
		}
		else {
			return scalar p_`s'_warn = 0
		}
		
		return scalar p_`s' = `plist_`s'_vcount'
	}
	
	************************************
	** Check whether the treatment effect was identified
	************************************
	if _b[`d'] == 0 & _se[`d'] == 0 {
		di "{err}Insufficient data to compute estimates"
		exit 99
	}
	else {
		**return the parameter estimate to the caller
		return scalar b = _b[`d']
	}
		
	***************************
	** Count observations included in the regression
	** We don't need to do this when we're bootstrapping
	** so make it optional
	***************************
	if "`count'" != "nocount" {
		qui count if  `w' != 0 & `rv' < `c'
		return scalar N_l = r(N)

		qui count if  `w' != 0 & `rv' > `c' & `if' 
		return scalar N_r = r(N)
	}
	
	
end



	
	
