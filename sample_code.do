clear
set more off
set obs 1000

do "I:\Personal Files\Jim\Stata\Git\RegressionDiscontinuity\RegressionDiscontinuity.ado"

drawnorm rv uv, cov(1,.5\.5,1)

gen e = rnormal()

gen d = rv > 1

gen y = 0

forvalues i=1/3 {
	scalar a`i' = runiform()-0.5
	replace y = y + a`i'*rv^`i'
}

replace y = y + d + uv + e

reg y rv i.d, robust


rdrobust y rv, c(1) p(1) covs(uv) kernel(tri)
scalar bwl = e(h_l)
scalar bwr = e(h_r)


*RegressionDiscontinuity y rv, c(1) bwidth(`=bwl' `=bwr') p(1) bsreps(100) cov(uv)
**Todo: Covariates don't replicate rdrobust. I don't know how they deal with them
**Compute kernel weights for each observation
local c 1
local rv rv
tempvar touse
gen byte `touse' = 1
tempvar w rvl rvr d
qui gen `w' = 1 - abs( (`c'-`rv')/bwl )  if inrange( (`c'-`rv')/bwl, 0, 1) & `touse'
qui replace `w' = 1 - abs( (`c'-`rv')/bwr )  if inrange( (`c'-`rv')/bwr, -1, 0) & `touse'
replace `w' = 0 if `w' == .
gen w = `w'

gen `rvl' = cond(`rv'<`c',`rv'-`c',0)
gen `rvr' = cond(`rv'>`c',`rv'-`c',0)
gen byte `d' = `rv' > `c'

reg y uv `rvl' `rvr' d [aw=`w']




rdrobust y rv, c(1) p(1) kernel(tri) covs(uv)
RegressionDiscontinuity y rv, c(1) bwidth(`=e(h_l)' `=e(h_r)') p(1) bsreps(100) cov(uv) kernel(triangle)
estimates store SAMPLE1
estimates restore SAMPLE1


rdrobust y rv, c(1) p(2) kernel(tri) covs(uv)
RegressionDiscontinuity y rv, c(1) bwidth(`=e(h_l)' `=e(h_r)') p(2) bsreps(100) cov(uv) kernel(triangle)
RegressionDiscontinuity y rv, c(1) bwidth(`=e(h_l)' `=e(h_r)') p(2 1) bsreps(100) cov(uv) kernel(triangle)
